// create application
import { createApp } from "vue"
import App from "./App.vue"
const app = createApp(App)

// add amplify
import { Amplify } from "aws-amplify"
import awsExports from "@/aws-exports"
Amplify.configure(awsExports)

// add pinia
import { createPinia } from "pinia"
app.use(createPinia())

// add router
import router from "./router"
app.use(router)

// add vuetify
import Vuetify from "@/plugins/vuetify"
import "@mdi/font/css/materialdesignicons.css"
import "vuetify/styles"
app.use(Vuetify)

app.mount("#app")

// add css
import "./assets/main.css"
