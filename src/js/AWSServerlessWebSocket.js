//https://3qkb1ureud.execute-api.eu-west-1.amazonaws.com/test/@connections

const wait = milliseconds => new Promise((resolve) => setTimeout(resolve, milliseconds))

const AWSServerlessWebSocket = class {
  constructor(url, options) {
    this.url = url
    this.options = options

    this.subscriptions = new Map()

    this.ws = new WebSocket(this.url)

    this.ws.addEventListener("error", console.error)

    this.ws.addEventListener("open", () => {
      this.isReady = true
    })

    this.ws.addEventListener("message", (event) => {
      const body = JSON.parse(event.data)
      try {
        const message = JSON.parse(body.message)

        let callback

        if (body.channel) {
          callback = this.subscriptions.get(body.channel)
        }

        if (callback === undefined) {
          callback = this.subscriptions.get(undefined)
        }

        if (callback) {
          callback(message)
        }
      } catch(error) {
        console.log(body)
      }
    })
  }

  close() {
    this.ws.close()
  }

  async ready() {
    while (this.isReady !== true) {
      await wait(50)
    }
  }

  async _send(object) {
    await this.ready()
    this.ws.send(JSON.stringify(object))
  }

  async subscribe(callback) {
    if (this.subscriptions.has(undefined) === false) {
      await this._send({
        action: "subscribe",
        application: this.options.application,
        client: this.options.client
      })

      this.subscriptions.set(undefined, callback)
    }
  }

  async subscribeToChannel(channel, callback) {
    if (this.subscriptions.has(channel) === false) {
      await this._send({
        action: "subscribe",
        application: this.options.application,
        client: this.options.client,
        channel
      })

      this.subscriptions.set(channel, callback)
    }
  }

  sendMessage(client, channel, message) {
    return this._send({
      action: "send",
      application: this.options.application,
      from: this.options.client,
      client,
      channel,
      message: JSON.stringify(message)
    })
  }

  sendMessageToClient(client, message) {
    return this.sendMessage(client, undefined, message)
  }

  sendMessageToChannel(channel, message) {
    return this.sendMessage(undefined, channel, message)
  }
}

export default AWSServerlessWebSocket
