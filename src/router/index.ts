import { createRouter, createWebHistory } from "vue-router"

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: () => import("@/views/trivia/TriviaGame.vue")
    },
    {
      path: "/trivia",
      name: "trivia-game",
      component: () => import("@/views/trivia/TriviaGame.vue")
    },
    {
      path: "/trivia/help",
      name: "trivia-help",
      component: () => import("@/views/trivia/TriviaHelp.vue")
    },
    {
      path: "/trivia/leaderboard",
      name: "trivia-leaderboard",
      component: () => import("@/views/trivia/TriviaLeaderboard.vue")
    },
    {
      path: "/trivia/administration",
      name: "trivia-administration",
      component: () => import("@/views/trivia/TriviaAdministration.vue")
    }
  ]
})

export default router
