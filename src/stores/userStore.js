import { defineStore } from "pinia"
import { ref } from "vue"

export const useUserStore = defineStore("user", () => {
  const user = ref(null)
  const isAdministrator = ref(false)

  const signedIn = thisUser => {
    user.value = thisUser
    isAdministrator.value = thisUser.groups && thisUser.groups.includes("administrators")
  }

  const signedOut = () => {
    user.value = null
    isAdministrator.value = false
  }

  return { user, isAdministrator, signedIn, signedOut }
})
