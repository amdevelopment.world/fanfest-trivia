import { defineStore } from "pinia"
import { ref } from "vue"

const countScore = results => {
  let counter = 0
  results.forEach(result => {
    if(result) {
      counter++
    }
  })

  return counter
}

export const useMatchStore = defineStore("match", () => {
  const match = ref(null)

  const startMatch = (value, userId) => {
    match.value = value
    const isA = value.player.A.id === userId
    match.value.me = isA ? value.player.A : value.player.B
    match.value.opponent = isA ? value.player.B : value.player.A
    match.value.questionIndex = 0
  }

  const nextQuestion = () => {
    match.value.questionIndex++
  }

  const finishMatch = () => {
    match.value = undefined
  }

  const addResult = (message) => {
    const {playerLetter, itsTheCorrectAnswer, matchFinished} = message

    match.value.finished = matchFinished
    match.value.player[playerLetter].results.push(itsTheCorrectAnswer)

    if(matchFinished) {
      const myScore = countScore(match.value.me.results)
      const opponentScore = countScore(match.value.opponent.results)

      if(myScore == opponentScore) {
        match.value.result = "draw"
      } else if(myScore > opponentScore) {
        match.value.result = "win"
      } else {
        match.value.result = "loss"
      }
    }
  }

  return { match, startMatch, finishMatch, addResult, nextQuestion }
})
