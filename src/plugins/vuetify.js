import { createVuetify } from "vuetify"
import "@mdi/font/css/materialdesignicons.css"
import "vuetify/styles"
import * as components from "vuetify/components"
import * as directives from "vuetify/directives"
import * as labs from "vuetify/labs/components"

const customDarkTheme = {
  dark: true,
  colors: {
    background: "#15202b",
    surface: "#15202b",
    primary: "#5E38BD",
    secondary: "#FF5F1F",
    error: "#f44336",
    info: "#2196F3",
    success: "#4caf50",
    warning: "#fb8c00"
  }
}

export default createVuetify({
  components: {
    ...components,
    ...labs
  },
  directives,
  theme: {
    defaultTheme: "customDarkTheme",
    themes: {
      customDarkTheme
    }
  }
})
