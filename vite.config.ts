import { defineConfig } from "vite"
import vue from "@vitejs/plugin-vue"
import path from "path"

console.log(path.resolve(__dirname, "src/components/icons"))

export default defineConfig({
  plugins: [
    vue()
  ],
  resolve: {
    alias: {
      "@": path.resolve(__dirname, "src"),
      "@trivia": path.resolve(__dirname, "src/components/trivia"),
      "@components": path.resolve(__dirname, "src/components")
    }
  }
})
