const { MongoClient, ObjectId } = require("mongodb")
const { CognitoIdentityProviderClient, AdminGetUserCommand } = require("@aws-sdk/client-cognito-identity-provider")

const log = (...args) => {
  console.log(JSON.stringify(args, undefined, "  "))
}

const Persistence = class {
  async connect() {
    if(this.client === undefined) {
      this.client = await MongoClient.connect(process.env.MONGO_URL, {
      })

      this.db = this.client.db("trivia")

      this.matches = await this.collectionWithIndexes("matches", [
        { "date": 1 },
        { "player.A.id": 1 },
        { "player.B.id": 1 }
      ])

      this.questions = await this.collectionWithIndexes("questions", [
        { "question": 1 , options: { unique: true } },
      ])

      this.players = await this.collectionWithIndexes("players", [
        { "playerId": 1, options: { unique: true } },
        { "numberOfMatchesWon": 1 },
        { "numberOfMatchesLost": 1 },
        { "numberOfMatchesPlayed": 1 },
        { "numberOfQuestionsAnsweredWrongly": 1 },
        { "numberOfQuestionsAnsweredCorrectly": 1 }
      ])

      this.cognitoClient = new CognitoIdentityProviderClient({
        region: process.env.REGION
      })
    }
  }

  // general methods

  async getDocuments(collectionName, skip, limit, sort) {
    await this.connect()
    const collection = await this.db.collection(collectionName)
    const cursor = await collection.find()

    if(skip !== undefined) {
      cursor.skip(skip)
    }
    
    if(limit !== undefined) {
      cursor.limit(limit)
    }

    if(sort !== undefined) {
      cursor.sort(sort)
    }

    return cursor.toArray()
  }

  async getDocument(collectionName, query, projection) {
    await this.connect()
    const collection = await this.db.collection(collectionName)

    if(query.id) {
      query._id = new ObjectId(query.id)
      delete query.id
    }
    
    return collection.findOne(query, projection)
  }

  async createDocument(collectionName, doc) {
    await this.connect()
    const collection = await this.db.collection(collectionName)
    return collection.insertOne( doc)
  }

  async updateDocument(collectionName, query, update) {
    await this.connect()
    const collection = await this.db.collection(collectionName)
    return collection.updateOne(query, {
      $set: update
    })
  }

  async deleteDocumentByID(collectionName, id) {
    await this.connect()
    const collection = await this.db.collection(collectionName)
    return collection.deleteOne({
      _id: new ObjectId(id)
    })
  }

  async deleteDocuments(collectionName, documents) {
    await this.connect()
    const collection = await this.db.collection(collectionName)
    
    const fields = {}

    documents.forEach(doc => {
      for(let name in doc) {
        let value = doc[name]

        // map field name
        name = {
          "id": "_id"
        }[name] || name

        if(fields[name] === undefined) {
          fields[name] = []
        }

        if(name === "_id") {
          value = new ObjectId(value)
        }

        fields[name].push(value)
      }
    })

    const query = {$or:[]}
  
    for(const name in fields) {
      const values = fields[name]

      query.$or.push({
        [name]: {$in: values}
      })
    }

    return collection.deleteMany(query)
  }

  // players
  async getCognitoPlayer(Username) {
    const user = await this.cognitoClient.send(
      new AdminGetUserCommand({
        Username,
        UserPoolId: process.env.COGNITO_USERPOOL_ID
      })
    )

    const attributes = {}
    const includeAttributes = {
      nickname :true
    }

    for(const UserAttribute of user.UserAttributes) {
      if(includeAttributes[UserAttribute.Name]) {
        attributes[UserAttribute.Name] = UserAttribute.Value
      }
    }

    return {
      playerId: Username,
      ...attributes
    }
  }

  async incrementPlayerCounters(playerIds, counters) {
    return this.players.updateOne({
      playerId: {$in: playerIds}
    }, {
      $inc: counters
    })
  }

  async getPlayer(playerId, projection) {
    let player = await this.players.findOne({
      playerId
    }, projection)

    if(player == undefined) {
      player = await this.getCognitoPlayer(playerId)
      await this.players.insertOne(player)
    }

    return player
  }

  // matches
  async createMatchForPlayer(player) {
    await this.connect()

    const match = {
      date: new Date(),
      player: {
        A: {
          id: player.playerId,
          nickname: player.nickname,
          results: []
        }
      }
    }

    await this.matches.insertOne(match)
    match.id = match._id.toString()

    return match
  }

  async addResultToMatch(matchId, playerLetter, result) {
    return this.matches.findOneAndUpdate({
      _id: new ObjectId(matchId)
    }, {
      $push: {
        [`player.${playerLetter}.results`]: result
      }
    }, {
      returnDocument: "after"
    })
  }

  async findAvailableMatch(userId) {
    await this.connect()
  
    const documents = await this.matches.aggregate([
      { $match: {
        ["player.A.id"]: { $ne: userId },
        ["player.B.id"]: { $exists: false }
      } },
      { $sample: { size: 1 } }
    ]).toArray()
  
    return documents[0]
  }

  // questions
  async addQuestions(questions) {
    await this.connect()

    const operations = questions.map(question => {
      return {
        updateOne: {
          filter: { question:question.question },
          update: {
            $set: question
          },
          upsert: true
        }
      }
    })

    await this.questions.bulkWrite(operations)
  }

  getRandomQuestions(numberOfQuestions) {
    return this.questions.aggregate([
      { $sample: { size: numberOfQuestions } }
    ]).toArray()
  }

  // mongo
  async close() {
    if(this.client) {
      const client = this.client
      delete this.client
      await client.close()
    }
  }

  async collectionNamed (name) {
    let collection = await this.db.collection(name)

    if (collection === undefined) {
      collection = await this.db.createCollection(name)
    }

    return collection
  }

  async collectionWithIndexes (name, indexes) {
    const collection = await this.collectionNamed(name)
    indexes = indexes || []

    for (const index of indexes) {
      const options = index.options

      if (options) {
        delete index.options
        await collection.createIndex(index, options)
      } else {
        await collection.createIndex(index)
      }
    }

    return collection
  }
}

module.exports = Persistence