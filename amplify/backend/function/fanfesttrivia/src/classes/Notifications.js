const WebSocket = require("ws")

const application = "trivia"

const log = (...args) => {
  console.log(JSON.stringify(args, undefined, "  "))
}

const Notifications = class {
  constructor(url) {
    this.url = url
  }

  async ready() {
    if(this.isReady !== undefined) {
      return this.isReady
    }

    return new Promise(resolve => {
      this.ws = new WebSocket(this.url, {
        perMessageDeflate: true
      })

      this.ws.on("error", () => {
        this.isReady = false
        resolve(this.isReady)
      })

      this.ws.on("open", () => {
        this.isReady = true
        resolve(this.isReady)
      })
    })
  }

  close() {
    return new Promise(resolve => {
      this.ws.close()

      this.ws.on("close", () => {
        this.isReady = false
        resolve()
      })
    })
  }

  async send(payload) {
    if(await this.ready() !== true) {
      return
    }

    return new Promise((resolve, reject) => {
      this.ws.send(JSON.stringify(payload), error => {
        if(error) {
          reject(error)
        } else {
          resolve()
        }
      })
    })
  }

  sendResultToPlayers(result, clients) {
    return this.send({
      action: "send",
      application,
      clients,
      message: JSON.stringify({
        action: "addResult",
        ...result
      })
    })
  }

  sendMatchToPlayers(match, clients) {
    console.log("Notifications.sendMatchToPlayers",match, clients)

    return this.send({
      action: "send",
      application,
      clients,
      message: JSON.stringify({
        action: "startMatch",
        match
      })
    })
  }
}

module.exports = Notifications