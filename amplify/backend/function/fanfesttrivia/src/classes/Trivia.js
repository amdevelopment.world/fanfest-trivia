const Notifications = require("./Notifications")
const Persistence = require("./Persistence")

const notifications = new Notifications(process.env.AWS_SERVERLESS_WEBSOCKETS_URL)
const persistence = new Persistence(process.env.MONGO_URL)

const log = (...args) => {
  console.log(JSON.stringify(args, undefined, "  "))
}

const Trivia = class {
  async close() {
    await persistence.close()
    await notifications.close()
  }

  // any
  get(collectionName, skip, limit, sort) {
    return persistence.getDocuments(collectionName, skip, limit, sort)
  }

  getOne(collectionName, query) {
    return persistence.getDocument(collectionName, query)
  }

  update(collectionName, query, update) {
    return persistence.updateDocument(collectionName, query, update)
  }

  create(collectionName, doc) {
    return persistence.createDocument(collectionName, doc)
  }

  // matches
  async startMatch(playerId) {
    const availableMatch = await persistence.findAvailableMatch(playerId)
    const player = await persistence.getPlayer(playerId)

    if(availableMatch !== undefined) {
      const questions = await persistence.getRandomQuestions(5)

      const playerB = {
        id: player.playerId,
        nickname: player.nickname,
        results: []
      }

      const update = {
        ["player.B"]: playerB,
        questions: questions.map(question => {
          return {
            id: question._id.toString(),
            question: question.question
          }
        })
      }

      await persistence.updateDocument("matches", {
        _id: availableMatch._id
      }, update)
      
      availableMatch.player.B = playerB
      availableMatch.questions = update.questions

      const playerAId = availableMatch.player.A.id
      const playerBId = availableMatch.player.B.id

      availableMatch.id = availableMatch._id.toString()
      delete availableMatch._id

      await notifications.sendMatchToPlayers(availableMatch, [
        playerAId, playerBId
      ])

      return {success: true}
    } else {
      const match = await persistence.createMatchForPlayer(player)
      return {
        success: true,
        matchId: match.id
      }
    }
  }

  async deleteMatch(id) {
    await persistence.deleteDocumentByID("matches", id)
    return {success: true}
  }

  // questions
  async addQuestions(questions) {
    await persistence.addQuestions(questions)
    return {success: true}
  }

  async deleteQuestions(questions) {
    await persistence.deleteDocuments("questions", questions)
    return {success: true}
  }

  // answers
  async submitAnswer(matchId, playerId, questionId, givenAnswer) {
    const question = await this.getOne("questions", {id: questionId})
    let match = await this.getOne("matches", {id: matchId})

    // check answer
    const answers = typeof question.answer === "object" ? question.answer : [question.answer]
    let itsTheCorrectAnswer = false

    for(const answer of answers) {
      if((givenAnswer.toLowerCase().indexOf(answer.toLowerCase())) !== -1) {
        itsTheCorrectAnswer = true
      }
    }

    const playerLetter = match.player.A.id == playerId ? "A" : "B"

    // update the match results
    match = await persistence.addResultToMatch(matchId, playerLetter, itsTheCorrectAnswer)

    const matchFinished = (match.player.A.results.length === match.player.B.results.length && match.player.A.results.length === match.questions.length)

    const countBooleans = booleans => {
      let counter = 0

      for(const boolean of booleans) {
        if(boolean) {
          counter++
        }
      }

      return counter
    }

    // update the player results
    if(matchFinished) {
      const playerAScore = countBooleans(match.player.A.results)
      const playerBScore = countBooleans(match.player.B.results)

      if(playerAScore == playerBScore) {
        await persistence.incrementPlayerCounters([match.player.A.id, match.player.B.id], {
          numberOfMatchesPlayed: 1,
        })
      } else {
        await persistence.incrementPlayerCounters([match.player.A.id], {
          numberOfMatchesPlayed: 1,
          [playerAScore >  playerBScore ? "numberOfMatchesWon" : "numberOfMatchesLost"]: 1
        })

        await persistence.incrementPlayerCounters([match.player.B.id], {
          numberOfMatchesPlayed: 1,
          [playerBScore >  playerAScore ? "numberOfMatchesWon" : "numberOfMatchesLost"]: 1
        })
      }
    }

    await persistence.incrementPlayerCounters([playerId], {
      [itsTheCorrectAnswer ? "numberOfQuestionsAnsweredCorrectly" : "numberOfQuestionsAnsweredWrongly"]: 1
    })

    await notifications.sendResultToPlayers({
      playerLetter,
      itsTheCorrectAnswer,
      matchFinished
    }, [match.player.A.id, match.player.B.id]) 

    return {success: true}
  }

  onConnectWebSocket(connectionId) {
    console.log("onConnectWebSocket", connectionId)
  }

  onDisconnectWebSocket(connectionId, clients) {
    console.log("onDisconnectWebSocket", connectionId, clients)
  }

  onMessageFromWebSocket(message) {
    console.log("onMessageFromWebSocket", message)
  }
}

module.exports = Trivia