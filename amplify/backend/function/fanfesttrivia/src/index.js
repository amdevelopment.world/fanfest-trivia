const app = require("./app")

const AWSServerlessWebSockets = require("./AWSServerlessWebSockets")
const awsServerlessExpress = require("aws-serverless-express")
const server = awsServerlessExpress.createServer(app)

const awsServerlessWebSockets = new AWSServerlessWebSockets()

exports.handler = async (event, context) => {
  console.log(`EVENT: ${JSON.stringify(event)}`)
  try {
    const response = await awsServerlessWebSockets.handleRequest(event,
      connectionId => app.trivia.onConnectWebSocket(connectionId),
      (connectionId, clientId) => app.trivia.onDisconnectWebSocket(connectionId, clientId),
      message => app.trivia.onMessageFromWebSocket(message))
    if(response) {
      // this is a $connect, $disconnect or $default event for our websocket
      console.log("handleRequest returned", response)
      return response
    } else {
      console.log("handleRequest returned undefined for", event)
      event.headers.cognitoUserId = event.requestContext.identity.cognitoAuthenticationProvider.split("CognitoSignIn:")[1]
      return awsServerlessExpress.proxy(server, event, context, "PROMISE").promise
    }
  } catch(error) {
    console.log("------------------------ error in trivia lambda function ------------------------")
    console.error(error)
  }
}