// the following comment is edited by amplify when ENV vars are added or removed

/* Amplify Params - DO NOT EDIT
  ENV
  REGION
Amplify Params - DO NOT EDIT */

const express = require("express")
const bodyParser = require("body-parser")
const awsServerlessExpressMiddleware = require("aws-serverless-express/middleware")
const Trivia = require("./classes/Trivia")
const trivia = new Trivia()

// declare a new express app
const app = express()
app.trivia = trivia

app.use(bodyParser.json())
app.use(awsServerlessExpressMiddleware.eventContext())

// Enable CORS
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "*")
  next()
})

// security
app.all("*", async function (req, res, next) {
  req.cognitoId = req.get("cognitoUserId")
  next()
})

// --------------------------------------------------------- players ----------------------------------------

// get players
app.get("/trivia/players", async (req, res) => {
  const response = await trivia.get("players", req.params.skip, req.params.limit, {
    [req.params.sort]: req.params.ascending ? -1 : 1
  })
  res.json(response)
})

// get the connected player
app.get("/trivia/player", async (req, res) => {
  const response = await trivia.getOne("players", {
    playerId: req.cognitoId
  })
  res.json(response)
})

// change nickname of the connected player
app.put("/trivia/player/nickname", async (req, res) => {
  const response = await trivia.update("players", {
    playerId: req.cognitoId
  }, {
    nickname: req.body.nickname
  })
  res.json(response)
})

// --------------------------------------------------------- matches ----------------------------------------

// get matches
app.get("/trivia/matches", async (req, res) => {
  const response = await trivia.get("matches", req.params.skip, req.params.limit, {
    [req.params.sort]: req.params.ascending ? -1 : 1
  })
  res.json(response)
})

// get one match
app.get("/trivia/match/:id", async (req, res) => {
  const response = await trivia.getOne("matches", {
    id: req.params.id
  })
  res.json(response)
})

// create, or find a match
app.post("/trivia/match", async (req, res) => {
  const response = await trivia.startMatch(req.cognitoId)
  res.json(response)
})

// delete a match by id
app.delete("/trivia/match/:id", async (req, res) => {
  const response = await trivia.deleteMatch(req.params.id)
  res.json(response)
})

// --------------------------------------------------------- questions ---------------------------------------

// get questions
app.get("/trivia/questions", async (req, res) => {
  const response = await trivia.get("questions", req.params.skip, req.params.limit, {
    [req.params.sort]: req.params.ascending ? -1 : 1
  })
  res.json(response)
})

// get one question
app.get("/trivia/question/:id", async (req, res) => {
  const response = await trivia.getOne("questions", {
    id: req.params.id
  })
  res.json(response)
})

// import an array of questions
app.post("/trivia/questions", async (req, res) => {
  const response = await trivia.addQuestions(req.body)
  res.json(response)
})

// delete a single question by id
app.delete("/trivia/question/:id", async (req, res) => {
  const response = await trivia.deleteQuestions([{
    id: req.params.id
  }])
  res.json(response)
})

// delete multiple questions, by id, or by question
app.delete("/trivia/questions/:id", async (req, res) => {
  const response = await trivia.deleteQuestions(req.body)
  res.json(response)
})

// --------------------------------------------------------- answers ---------------------------------------

app.put("/trivia/answer", async (req, res) => {
  const response = await trivia.submitAnswer(req.body.matchId, req.cognitoId, req.body.questionId, req.body.answer)
  res.json(response)
})

app.listen(3000, function() {
  console.log("App started")
})

module.exports = app
