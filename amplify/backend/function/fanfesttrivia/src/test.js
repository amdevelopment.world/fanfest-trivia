try {
  //require("./env.js")
  require("./localenv.js")
} catch(error) {
  console.log("create a env.js file here settings values for process.env.MONGO_URL and AWS_SERVERLESS_WEBSOCKETS_URL")
}

const log = (...args) => {
  console.log(JSON.stringify(args, undefined, "  "))
}

if(process.env.MONGO_URL) {
  const Trivia = require("./classes/Trivia")
  const trivia = new Trivia()

  const testAddQuestions = async () => {
    let response

    response = await trivia.addQuestions([
      {
        question: "What is the Capital of France ?",
        answer: "Paris"
      }, {
        question: "What is the Capital of Burkina Faso ?",
        answer: "Ouagadougou"
      }, {
        question: "What is the Capital of the Netherlands ?",
        answer: "Hague"
      }, {
        question: "1+1 ?",
        answer: "2"
      }, {
        question: "Whats the Highest Mountain on Earth ?",
        answer: "Everest"
      }, {
        question: "Did Oppenheimer win the Oscar ?",
        answer: "Yes"
      }, {
        question: "Would it be better to have Multiple Choice Questions ?",
        answer: "Yes"
      }, {
        question: "What is the Capital of Europe ?",
        answer: "Brussels"
      }, {
        question: "How many bones are in the human body ?",
        answer: "206"
      }, {
        question: "What galaxy do we live in ?",
        answer: "Milky"
      }, {
        question: "What are diamonds made of ?",
        answer: "Carbon"
      }, {
        question: "What is a group of crows called ?",
        answer: "Murder"
      }, {
        question: "What is the largest desert in the world ?",
        answer: "Antartica"
      }, {
        question: "Who painted the Mona Lisa ?",
        answer: ["Leonardo", "Vinci"]
      }, {
        question: "Which animal is the tallest in the world ?",
        answer: "Giraffe"
      }, {
        question: "In an alphabetical list of US states, which comes last ?",
        answer: "Wyoming"
      }, {
        question: "What is the chemical symbol for potassium ?",
        answer: "K"
      }, {
        question: "Which letter is representative of 50 in Roman numerals ?",
        answer: "L"
      }
    ])

    log(response)
    
    const questions = await trivia.get("questions", 0, 5, {
      "question": 1
    })

    log(questions)

    /*
    response = await trivia.deleteQuestions([
      {
        question: "What is the Capital of France ?"
      }, {
        id: "65ef8a8baff44a619967259c"
      }
    ])
    log(response)
    */
  }

  const testPlayers = async() => {
    const players = await trivia.get("players", 0, 1)
    const playerId = players[0].playerId
    
    let player

    player = await trivia.getOne("players", {
      playerId
    })

    log(player)

    await trivia.update("players", {
      playerId
    }, {
      "nickname": "new nickname"
    })

    player = await trivia.getOne("players", {
      playerId
    })

    log(player)
  }
  const testMatch = async () => {
    const players = await trivia.get("players")
    const playerA = players[0]
    const playerB = players[1]
    debugger
    const responseA = await trivia.startMatch(playerA.playerId)
    log(responseA)

    const responseB = await trivia.startMatch(playerB.playerId)
    log(responseB)

    const match = await trivia.getOne("matches", {
      id: responseA.matchId
    })

    const getAnswer = question => {
      if(typeof question.answer === "object") {
        return question.answer[0]
      } else {
        return question.answer
      }
    }

    for (const [index, question] of match.questions.entries()) {
      const answer = await trivia.getOne("questions", {id: question.id})
      await trivia.submitAnswer(responseA.matchId, match.player.A.id, question.id, (index % 2) ? "wrong answer": getAnswer(answer))
      await trivia.submitAnswer(responseA.matchId, match.player.B.id, question.id, (index % 2) ? getAnswer(answer) :  "wrong answer")
    }

    // await trivia.deleteMatch(responseA.matchId)
  }

  const createPlayers = async () =>{
    try {
      await trivia.create("players", {
        "playerId" : "f43fb48d-354d-4766-9103-54487b43cf4b",
        "nickname" : "Mars",
      })

      await trivia.create("players", {
        "playerId" : "fbde399b-0986-4271-8d5b-7a9a9ade9732",
        "nickname" : "Rowin",
      })
    } catch(error) {
      console.log("players allready exist")
    }
  }

  const test = async () => {
    await createPlayers()
    await testAddQuestions()
    await testMatch()
    //await testPlayers()


    await trivia.close()
    console.log("closed")
  }
  test()
}