const { MongoClient } = require("mongodb")

const MongoDB = class {
  async connect() {
    if(this.client === undefined) {
      console.log("connecting to database")
      this.client = await MongoClient.connect(process.env.MONGO_URL, {
      })

      this.db = this.client.db("aws-serverless-websockets")
      this.collection = await this.collectionWithIndexes("clients", [
        { "connectionId": 1 },
        { "application": 1 },
        { "clientId": 1 },
        { "channel": 1 }
      ])
      console.log("connected to database")
    }
  }

  async close() {
    if(this.client) {
      const client = this.client
      delete this.client
      await client.close()
      console.log("disconnected from mongoDB")
    }
  }

  async $connect(connectionId, callback) {
    if(callback) {
      await callback(connectionId)
    }

    return "connected"
  }

  async $disconnect(connectionId, callback) {
    if(callback) {
      const documents = await this.collection.find({
        connectionId
      } , {
        connectionId: 1,
        client: 1
      }).toArray()

      console.log("$disconnect.documents", documents)
      const clients = documents.map(doc => doc.client)
      console.log("$disconnect.clients", clients)

      await callback(connectionId, clients)
    }

    await this.collection.deleteMany({
      connectionId
    })

    return "disconnected"
  }

  async subscribe(connectionId, body) {
    await this.collection.insertOne({
      connectionId,
      application: body.application,
      channel: body.channel,
      client: body.client
    })

    return "subscribed"
  }

  async getConnectionsFor(body) {
    const query = {
      application: body.application
    }

    if(body.channel) {
      query.channel = body.channel
    }

    if(body.client) {
      query.client = { $in: [body.client]}
    }

    if(body.clients) {
      query.client = { $in: body.clients}
    }

    const documents = await this.collection.find(query, {
      connectionId: 1,
      client: 1
    }).toArray()

    return documents
  }

  async collectionNamed (name) {
    let collection = await this.db.collection(name)

    if (collection === undefined) {
      collection = await this.db.createCollection(name)
    }

    return collection
  }

  async collectionWithIndexes (name, indexes) {
    const collection = await this.collectionNamed(name)
    indexes = indexes || []

    for (const index of indexes) {
      const options = index.options

      if (options) {
        delete index.options
        await collection.createIndex(index, options)
      } else {
        await collection.createIndex(index)
      }
    }

    return collection
  }
}

module.exports = MongoDB