const { ApiGatewayManagementApiClient, PostToConnectionCommand } = require("@aws-sdk/client-apigatewaymanagementapi")
const Persistence = require("./Persistence.js")

const log = (...args) => {
  console.log(JSON.stringify(args, undefined, "  "))
}

let persistence = new Persistence()

console.log("API_GATEWAY_ENDPOINT", process.env.API_GATEWAY_ENDPOINT)
const apiGatewayClient= new ApiGatewayManagementApiClient({
  endpoint: process.env.API_GATEWAY_ENDPOINT
})

const AWSServerlessWebSockets = class {
  async handleRequest(event, onConnectWebSocket, onDisconnectWebSocket, onMessageFromWebSocket) {
    let response

    log("AWSServerlessWebSockets.handleRequest", event)

    if(event.requestContext) {
      const {
        requestContext: { connectionId, routeKey },
      } = event

      switch(routeKey) {
        case "$connect":
          await persistence.connect()
          response = await persistence.$connect(connectionId, onConnectWebSocket)
          break

        case "$disconnect":
          await persistence.connect()
          response = await persistence.$disconnect(connectionId, onDisconnectWebSocket)
          break

        case "$default":
          await persistence.connect()
          response = await $default(persistence, connectionId, JSON.parse(event.body), onMessageFromWebSocket)
          break

        default:
          return
      }
    } else {
      return
    }

    return {
      "isBase64Encoded": false,
      "statusCode": 200,
      "headers": {},
      "body": response || ""
    }
  }
}

const $default = (persistence, connectionId, body, callback) => {
  switch(body.action) {
    case "subscribe":
      console.log("subscribe")
      return persistence.subscribe(connectionId, body)

    case "send":
      if(callback) {
        callback(body)
      }

      return send(persistence, body)
  }
}

const send = async (persistence, body) => {
  const connections = await persistence.getConnectionsFor(body)
  return sendMessage(connections, body)
}

const sendMessage = async (connections, body) => {
  for(const connection of connections) {
    body.client = connection.client

    const request = {
      Data: JSON.stringify(body),
      ConnectionId: connection.connectionId
    }

    try {
      const command = new PostToConnectionCommand(request)
      await apiGatewayClient.send(command)
    } catch(error) {
      console.error(error)
    }
  }
}

module.exports = AWSServerlessWebSockets