import getEnv from "./getEnv.js"
import { CloudFrontClient, CreateInvalidationCommand } from "@aws-sdk/client-cloudfront"
const cloudFrontClient = new CloudFrontClient()

const DistributionIds = {
  "dev": "EQYSRJ9EN5HMQ",
  "prod": "321"
}

const invalidateCloudfront = async (Items) => {
  const env = await getEnv()
  const DistributionId = DistributionIds[env]
  console.log(`invalidating CloudFront ${env} ${DistributionId} for ${env}`)

  try {
    return cloudFrontClient.send(new CreateInvalidationCommand({
      DistributionId,
      InvalidationBatch: {
        CallerReference: `CallerReference_${Date.now()}`,
        Paths: {
          Quantity: Items.length,
          Items
        }
      }
    }))
  } catch (error) {
    console.error(`error when invalidating cache ${error}`)
  }
}

await invalidateCloudfront(["/*"])
