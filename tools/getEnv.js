import util from "util"
import {exec} from "child_process"
const execAsync = util.promisify(exec)

const environment = {
  "dev": {
    "USERPOOL": "us-east-1_iPwEBaDiz"
  },
  "prod": {
    "USERPOOL": "us-east-1_Uxq09SCDY" 
  }
}

export default async () => {
  const result = await execAsync("amplify env list")
  const env = /\*(\w+)/.exec(result.stdout)[1]

  console.log(`Amplify env: ${env}`)
  process.env.USERPOOL = environment[env]?.USERPOOL

  return env
}